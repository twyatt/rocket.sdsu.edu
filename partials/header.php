				<div id="earth">
				
				<div class="inner">
					<a href="index" id="logo">
						<img alt="San Diego State University" src="images/logo_sdsu.gif">
						<!-- <img alt="San Diego State University Rocket Project" src="images/logo_rocket_project.gif"> -->
					</a>
					
					<div id="header-links">
						<ul>
							<li class="first">
								<a href="join">Join</a>
							</li>
							<li>
								<a href="members">Members</a>
							</li>
							<li>
								<a href="donate">Donate</a>
							</li>
						</ul>
					</div>
					
<?php if ($page == 'home') { ?>
					<div id="banner">
						<div class="description">
							<h3>Rocketeers</h3>
							<div class="item-content">
								Rocket Project featured on the SDSU homepage!
							</div>
							<div class="item-more">
								<a href="http://newscenter.sdsu.edu/sdsu_newscenter/news.aspx?s=73975">Read More</a>
							</div>
						</div>
					</div>
<?php } ?>
				</div>
				
				<div id="menu-container">
					<div class="inner">
<?php include('partials/menu.php'); ?>
					</div>
				</div>
				
				</div>
<?php
$title = 'Sponsors';
?>

<div class="page">
	<h1>Sponsors</h1>
	
	<div class="sponsors">
		<a class="sponsor-link" href="https://www.facebook.com/carl.tedesco" target="_blank">
			<div class="sponsor" style="background-image: url('images/carl.jpg');"></div>
		</a>
		<a class="sponsor-link" href="http://www.flometrics.com/" target="_blank">
			<div class="sponsor" style="background-image: url('images/flometrics.gif');"></div>
		</a>
		<a class="sponsor-link" href="http://www.wrotenlaw.com/" target="_blank">
			<div class="sponsor" style="background-image: url('images/wa.jpg');"></div>
		</a>
		<a class="sponsor-link" href="http://www.asana.com" target="_blank">
			<div class="sponsor" style="background-image: url('images/asana_logo.png');"></div>
		</a>
		<a class="sponsor-link" href="http://www.sdcomposites.com/" target="_blank">
			<div class="sponsor" style="background-image: url('images/sdc.jpg');"></div>
		</a>
		<a class="sponsor-link" href="http://asme.sdsu.edu/" target="_blank">
			<div class="sponsor" style="background-image: url('images/asme.png');"></div>
		</a>
		<a class="sponsor-link" href="http://www.netburner.com/" target="_blank">
			<div class="sponsor" style="background-image: url('images/netburner.jpg');"></div>
		</a>
		<a class="sponsor-link" href="http://www.goldenstatecellular.com/" target="_blank">
			<div class="sponsor" style="background-image: url('images/golden_state_cellular_logo.png');"></div>
		</a>
		<a class="sponsor-link" href="http://www.embeddedadventures.com" target="_blank">
			<div class="sponsor" style="padding-left: 65px; background-image: url('images/ea_logo.jpg');"></div>
		</a>
		<a class="sponsor-link" href="http://friendsofamateurrocketry.org" target="_blank">
			<div class="sponsor" style="background-image: url('images/far_logo.png');"></div>
		</a>
		<a class="sponsor-link" href="http://www.virgingalactic.com" target="_blank">
			<div class="sponsor" style="background-image: url('images/vg_logo.jpg');"></div>
		</a>
		<a class="sponsor-link" href="http://www.northropgrumman.com" target="_blank">
			<div class="sponsor" style="background-image: url('images/ng_logo.png');"></div>
		</a>
		<a class="sponsor-link" href="http://mysolar.cat.com" target="_blank">
			<div class="sponsor" style="background-image: url('images/st_logo.jpg');"></div>
		</a>
		<a class="sponsor-link" href="http://www.nasa.gov" target="_blank">
			<div class="sponsor" style="background-image: url('images/nasa_logo.png');"></div>
		</a>
		<a class="sponsor-link" href="http://www.ni.com/academic" target="_blank">
			<div class="sponsor" style="background-image: url('images/ni_logo.jpg');"></div>
		</a>
		
		<div class="clear"></div>
	</div>
</div>